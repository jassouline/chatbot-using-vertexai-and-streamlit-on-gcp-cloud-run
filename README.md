# ChatBot using VertexAI and Streamlit on GCP Cloud Run

## Google Disclaimer

This is not an officially supported Google Product

## Introduction

This application was created to demonstrate how easy it is to interact with the PALM2 API in VertexAI to create a high-performance Chatbot.

![App Screen](image/demo.gif "Chat App with Streamlit & PALM2")

To achieve this, I used the following technologies:
- Cloud Run
- Cloud Build
- Artifact Registry
- Vertex AI
- PALM
- Streamlit Framework
- Python 3.11

## Content of this repository

In this repo you can find the following file :
```bash
.
├── Dockerfile
├── README.md
├── image
│   └── demo.gif
├── main.py
├── requirements.txt
└── setup.sh
``` 

The main.py is the Python code of the ChatBot using Vertex AI SDK & Streamlit framework.

The Dockerfile allow to containerized the ChatBot to be able to push it easily on Cloud Run.

The requirements.txt show the list of necessary librairies for Python execution.

The setup.sh is a Bash script to be run on Cloud Shell on GCP to setup all the ChatBot App.

## How to deploy the Chatbot App ?

It's really easy thanks to the setup.sh script.

1. Open CloudShell on your GCP Console 

![Cloud Shell](image/Cloud_Shell.webp)

1. Clone the repository
    ```bash
    git clone https://gitlab.com/google-cloud-ce/googlers/jassouline/genai_palm_chat.git
    ```

3. Edit the **setup.sh** file to set the value of your <PROJECT_ID>
    ```bash
    PROJECT_ID=""    #Change this value
    LOCATION="us-central1"
    ARTIFACT_REGISTRY_REPO="chatbot-palm-streamlit-repo"
    SERVICE_NAME="chatbot-palm-streamlit-app"
    CHATMODEL="chat-bison@001"
    ```

4. Make the file executable

    ```bash
    cd genai_palm_chat
    chmod a+x setup.sh
    ```

5. Run the script 

    ```bash
    ./setup.sh
    ```

## What are the steps performed by the setup.sh script ?

1. Check if the value of PROJECT_ID is set on the file setup.sh
   
2. Check if the Cloud Shell user is authenticated
   
3. Check if the PROJECT_ID exists
   
4. Check if the currend environment of Cloud Shell is configured for the project
   
5. Check if the GCP project is not enforced with the constraint "iam.allowedPolicyMemberDomains"
   
6. Change the value of PROJECT_ID, LOCATION & CHATMODEL into the main.py python file according to the value of the script setup.sh
   
7. Enable Artifact Registry, Cloud Build, Cloud Run, Vertex AI & Compute Engine APIs
   
8. Create a new Artifact Repository for the App
   
9.  Setup artefact Docker Authentication
    
10. Build the Docker image of the App with Cloud Build
    
11. Deploy the image on Cloud Run
    
12. Allow Cloud Run to accept incomming unauthenticated requests
    
13. Add the right IAM roles for the Default Compute Service Account
    
14. Show the Cloud Run URL of the App
