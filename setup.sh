#!/bin/bash
# Author : ASSOULINE Jordan
# Email : jordan.assouline@hotmail.fr
# Version : v1.0

# Set the project ID
PROJECT_ID=""
LOCATION="us-central1"
ARTIFACT_REGISTRY_REPO="chatbot-bison-streamlit-repo"
SERVICE_NAME="chatbot-bison-streamlit-app"
CHATMODEL="chat-bison"

function check_if_project_id_is_setup() {
    if [ -z "$PROJECT_ID" ]; then
        echo "Error: You must configure your PROJECT_ID."
        exit 1
    fi
}


function check_gcloud_authentication() {
    # Check if the user is authenticated with gcloud
    local AUTHENTICATED_USER=$(gcloud auth list --format="value(account)" --filter="status:ACTIVE")

    if [ -z "$AUTHENTICATED_USER" ]; then
    echo "No authenticated user found. Please authenticate using 'gcloud auth login'."
    exit 1
    else
    echo "Authenticated user is: $AUTHENTICATED_USER"
    fi
}

function check_gcp_project() {
 # Check if the project exists
 local PROJECT_NAME=$(gcloud projects describe $PROJECT_ID --format="value(name)" 2>/dev/null)

 if [ -z "$PROJECT_NAME" ]; then
   echo "Project $PROJECT_ID does not exist."
   exit 1
 else
   echo "Project $PROJECT_ID exists."
 fi

 # Check if the environment is configured for the project
 local CONFIGURED_PROJECT_ID=$(gcloud config get-value project 2>/dev/null)

 if [ "$CONFIGURED_PROJECT_ID" != "$PROJECT_ID" ]; then
   echo "Current environment is not configured for project $PROJECT_ID. Please run 'gcloud config set project $PROJECT_ID'."
   exit 1
 else
   echo "Environment is configured for project $PROJECT_ID."
 fi
}


function check_gcp_constraints() {
 local CONSTRAINTS=(
   "iam.allowedPolicyMemberDomains"
 )


 for CONSTRAINT in "${CONSTRAINTS[@]}"
 do
   local CONSTRAINT_STATUS=$(gcloud alpha resource-manager org-policies describe --effective --project=$PROJECT_ID $CONSTRAINT | sed 's/booleanPolicy: {}/ALLOW/' | grep -E 'constraint:|ALLOW' | awk '/ALLOW/ {print "allowed"}')


   if [ -z "$CONSTRAINT_STATUS" ]; then
     echo "Constraint $CONSTRAINT not found or not configured for this project."
     echo "Please ensure that the $CONSTRAINT constraint is authorized."
     exit 1
   elif [ "$CONSTRAINT_STATUS" = "allowed" ]; then
     echo "Constraint $CONSTRAINT is allowed."
   else
     echo "Constraint $CONSTRAINT is not allowed."
     echo "Please ensure that the $CONSTRAINT constraint is authorized."
     exit 1
   fi
 done
}


# Running checks before deploy
echo ""
echo "Running pre-checks"
echo ""
check_if_project_id_is_setup

# Check authentication
echo "***** Checking authentication with gcloud *****"
check_gcloud_authentication

# Check project configuration
echo "***** Checking project configuration *****"
check_gcp_project

# Check project constraints
echo "***** Checking project constraints *****"
check_gcp_constraints

# Change value of main.py environment variables
echo "***** Change Environment variabels of the main.py file *****"
function update_variables {
    # Update PROJECT_ID
    sed -i "s/^PROJECT_ID = .*$/PROJECT_ID = '$1'/" main.py

    # Update LOCATION
    sed -i "s/^LOCATION = .*$/LOCATION = '$2'/" main.py

    # Update CHATMODEL
    sed -i "s/^CHATMODEL = .*$/CHATMODEL = '$3'/" main.py
}

update_variables "$PROJECT_ID" "$LOCATION" "$CHATMODEL"


# Enable APIs
echo "***** Enabling Artifact Registry, Cloud Build, Cloud Run, Vertex AI, Compute APIs *****"
gcloud services enable artifactregistry.googleapis.com > /dev/null
gcloud services enable cloudbuild.googleapis.com > /dev/null
gcloud services enable run.googleapis.com > /dev/null
gcloud services enable aiplatform.googleapis.com > /dev/null
gcloud services enable compute.googleapis.com > /dev/null

echo "***** APIs enabled *****"

# Starting Configuration
echo "***** Create a new Artifact Repository for our webapp *****"
gcloud artifacts repositories create "$ARTIFACT_REGISTRY_REPO" --location="$LOCATION" --repository-format=Docker > /dev/null
echo "***** Repository created *****"

echo "***** Setup artefact docker authentication *****"
gcloud auth configure-docker "$LOCATION-docker.pkg.dev" --quiet > /dev/null

echo "***** Build WebApp Docker image *****"
gcloud builds submit --tag "$LOCATION-docker.pkg.dev/$PROJECT_ID/$ARTIFACT_REGISTRY_REPO/$SERVICE_NAME" > /dev/null

echo "***** Deploy on Cloud Run *****"
gcloud run deploy "$SERVICE_NAME" --port=8501 --image="$LOCATION-docker.pkg.dev/$PROJECT_ID/$ARTIFACT_REGISTRY_REPO/$SERVICE_NAME" --allow-unauthenticated --region=$LOCATION --platform=managed --project=$PROJECT_ID --set-env-vars=PROJECT_ID=$PROJECT_ID,LOCATION=$LOCATION > /dev/null
gcloud run services add-iam-policy-binding "$SERVICE_NAME" --member="allUsers" --role="roles/run.invoker" --region="$LOCATION" > /dev/null

echo "***** Retrieve default compute service account *****"
DEFAULT_COMPUTE_SA=$(gcloud compute project-info describe --format="value('defaultServiceAccount')")
echo "***** Bindind aiplatform.admin role to the Default Compute Service Account *****"
gcloud projects add-iam-policy-binding $PROJECT_ID --member=serviceAccount:${DEFAULT_COMPUTE_SA} --role=roles/aiplatform.admin > /dev/null

echo "***** Cloud RUN URL *****"
APP_URL=$(gcloud run services describe $SERVICE_NAME --region=us-central1 --format="value(status.url)")
echo $APP_URL




