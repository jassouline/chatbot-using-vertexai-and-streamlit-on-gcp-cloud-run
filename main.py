import streamlit as st
import vertexai
from vertexai.language_models import ChatModel

PROJECT_ID = 'chatbot-palm-streamlit'
LOCATION = 'us-central1'
CHATMODEL = 'chat-bison'

vertexai.init(project=PROJECT_ID, location=LOCATION)
chat_model = ChatModel.from_pretrained(CHATMODEL)
parameters = {
    "max_output_tokens": 1024,
    "temperature": 0.8,
    "top_p": 0.8,
    "top_k": 40
}
chat = chat_model.start_chat()


def generate_response(input_text):
    response = chat.send_message("""{}""".format(input_text), **parameters)
    return (response.text)


st.title('🦜🔗 Demo Chat with Bison')

st.sidebar.text('ChatModel used : ' + CHATMODEL)

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("What is up?"):
    # Add user message to chat history
    st.session_state.messages.append(
        {"role": "user",
         "content": prompt}
         )
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    with st.spinner('PaLM is working to generate, wait.....'):
        with st.chat_message("assistant"):
            response = generate_response(prompt)
            # Add assistant message to chat history
            st.session_state.messages.append(
                {"role": "assistant",
                 "content": response}
                 )
            st.markdown(response)
